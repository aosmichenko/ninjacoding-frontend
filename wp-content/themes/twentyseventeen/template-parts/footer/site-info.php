<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="contact-us-wrap">
    <a href="tel:+30987654321" class="btn-wrap btn-call-us">Call Us: <span>0987654321</span></a>
    <a href="mailto:testdomain@mail.to" class="btn-wrap btn-email-us">Email: <span>testdomain@mail.to</span></a>
    <a href="#" class="btn-wrap btn-contact-us">Contact Us</a>
</div>
<div class="site-info">
    <a href="<?php echo esc_url(__('https://wordpress.org/', 'twentyseventeen')); ?>"><?php printf(__('Proudly powered by %s', 'twentyseventeen'), 'WordPress'); ?></a>
</div><!-- .site-info -->
<div class="info-popup-wrap">
    <div class="info-popup">
        <div class="btn-close"></div>
        <div class="info-content-wrap">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolorum enim hic illum impedit, ipsam laborum
                non numquam officia officiis possimus quae ullam unde? Laborum modi mollitia non quo voluptatum!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolorum enim hic illum impedit, ipsam laborum
                non numquam officia officiis possimus quae ullam unde? Laborum modi mollitia non quo voluptatum!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolorum enim hic illum impedit, ipsam laborum
                non numquam officia officiis possimus quae ullam unde? Laborum modi mollitia non quo voluptatum!</p>
        </div>
    </div>
</div>